# window.kak

###### [Documentation] | [Source]

[source]: rc/window.kak
[documentation]: docs/window.asciidoc

Window commands for [Kakoune].

[kakoune]: https://kakoune.org

## Installation

```sh
git clone https://gitlab.com/vbauerster/window.kak.git ~/.config/kak/autoload/window
```
